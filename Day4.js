const start = 359282;
const end = 820401;

function isMonotone(pwd) {
  return pwd.split('').every((digit, index) => index === 0 || digit >= pwd[index - 1]);
}
function hasGroupOfTwo(pwd) { //erster Part
  return /(\d)\1/.test(pwd);
}
function hasGroupOfOnlyTwo(pwd) { // zweiter Part
  return (pwd.match(/(\d)\1+/g) || []).map(sequence => sequence.length).includes(2);
}
function isCandidate(pwd) {
  //function wechseln um erstes Ergebnis zu bekommen
  return hasGroupOfOnlyTwo(pwd) && isMonotone(pwd);
}

let validCount = 0;
for (let pwd = start; pwd <= end; pwd++) {
  validCount += isCandidate(String(pwd));
}

console.log(validCount);
