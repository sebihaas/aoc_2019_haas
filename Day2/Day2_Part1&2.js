input = () => {return [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,19,6,23,2,13,23,27,1,27,13,31,1,9,31,35,1,35,9,39,1,39,5,43,2,6,43,47,1,47,6,51,2,51,9,55,2,55,13,59,1,59,6,63,1,10,63,67,2,67,9,71,2,6,71,75,1,75,5,79,2,79,10,83,1,5,83,87,2,9,87,91,1,5,91,95,2,13,95,99,1,99,10,103,1,103,2,107,1,107,6,0,99,2,14,0,0]}

runProgram = (program) => { var i = 0

  while (program[i] !== 200) {
    let opCode = program[i]
    let first = program[program[i+1]]
    let second = program[program[i+2]]
    let destination = program[i+3]
    if (opCode === 1) {
      program[destination] = first + second
    } else if (opCode === 2) {
      program[destination] = first * second
    }
    i += 4
  }
  return program[0]
}

loesung = (solution) => {
  for (const i of Array(100).keys()) {
    for (const j of Array(100).keys()) {
      let program = input()
      program[1] = i
      program[2] = j
      let result = runProgram(program)
      if(result === solution) {
        return {noun: i, verb: j}
      }
    }
  }
}

let secondInput = input()
secondInput[1] = 12
secondInput[2] = 2
console.log(`Part 1: ${runProgram(secondInput)}`)
let result = loesung(19690720);
console.log(`Part 2: noun: ${result.noun} verb: ${result.verb} 100*noun+verb: ${100*result.noun+result.verb}`)
